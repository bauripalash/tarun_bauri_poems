বিচারেই ভরসা

ডোমের একটা চাকরি হবেক বিধি হল্য জারি

উচ্চ শিক্ষায় শিক্ষিতরা দাঁড়াই সারিসারি।

সনার বাংলার লেংটা রূপ ট দেখল্য জগৎ জুড়্যে

বিজ্ঞাপনে মুখ ঢাকলেও মুখট গেল পুড়্যে।

না খাঁয়ে আজ বেকার গুলান ঘুরছে কোর্টের পাশে

যোগ্য লোকের চাকরি হবেক এই দাবিটার আশে।

কাশের শিল্প, *ঘাসের* শিল্প, লাইগছে নাই আর ভালো

স্থায়ী কাজের স্বপুন বুকে খুঁজছে মানুষ আলো।

অল্প মাইনার চাকরি হলেও হছ্যে চুপিসারে

ঘুসের খেলা কেমন খেলা বুঝছ্যে ঠারে ঠারে

বিশ্বাস ট আজ হারাই *যাছে* লিজের মনের থাক্যে

শাক দিয়ে কি মাছ ঢাকা যায় যতই দ্যাখ চাখ্যে।

পরীক্ষাতে না বসে আজ মাস্টর হয়্যে যাছ্যে।

মন্ত্রী আমলার ভাই ভাতিজা বেশ ত কর‍্যে খাছ্যে

আদালত টাই হানা দিছে চোরের আতুড় ঘরে

সবাই কেমন সঁটক্যে আছে সিবি.আই এর ডরে

জজ সাহেবরা দাঁড়াই আছে মাথা উঁচু কর‍্যে

জেলখানা ট ভরায় দিবেক চোর গুলানকে ধর‍্যে

ঘুষ নামক এই মরণ খেলা চলত্যে থাকে যদি

যুব সমাজ উঠলে জাগ্যে লড়্যে যাবেক গদি

বাঁচার জন্য বাঁধছ্যে যে জোট সময় আছে হাতে

সময় থাকত্যে বুঝ না হয় মরব্যে প্রতিঘাতে।
