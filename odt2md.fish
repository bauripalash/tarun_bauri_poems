printf "ODT -> HTML"

for item in *.odt
    printf $item\n "-> html"
    pandoc $item -o output/$item.html
end

cd output

for file in *.html
    printf $file\n "-> md"
    pandoc $file -o md/$file.md
end

printf "Done!"
